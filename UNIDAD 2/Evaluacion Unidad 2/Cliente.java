import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class Cliente {

    private static DatagramSocket clientSocket;
    private static BufferedReader inFromUser;
    private static InetAddress IPAddress;
    private static Scanner scanner;
    private static DatagramPacket sendPacket;

    private static Double valor;
    private static String eleccionString;

    private static String host = "127.0.0.1";
    private static int puerto = 8000;

    public static void main(String[] args) throws SocketException, IOException, InterruptedException {
        clientSocket = new DatagramSocket();
        IPAddress = InetAddress.getByName(host);

        //byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];

        System.out.println("Enviar dato al servidor");
        boolean SystemFlag = true;
        System.out.println(" ------------- Bienvenido --------------");
        while (SystemFlag) {
            System.out.println("\n\nSELECCIONE UNA DE LAS SIGUIENTES OPCIONES:");
            System.out.println("0 => DEPOSITAR | 1 => RETIRAR | 2 => ESTADO DE CUENTA | 3 => SALIR");
            scanner = new Scanner(new InputStreamReader(System.in));
            int eleccion = scanner.nextInt();

            switch (eleccion) {
                case 0:
                    System.out.println("\n--> HA INGRESADO: DEPOSITAR");
                    scanner = new Scanner(new InputStreamReader(System.in));
                    valor = scanner.nextDouble();
                    eleccionString = "Depositar " + valor;
                    sendPacket = new DatagramPacket(eleccionString.getBytes(),
                            eleccionString.getBytes().length, IPAddress, puerto);
                    clientSocket.send(sendPacket);
                    break;

                case 1:
                    System.out.println("\n--> HA INGRESADO: RETIRAR");
                    scanner = new Scanner(new InputStreamReader(System.in));
                    valor = scanner.nextDouble();
                    eleccionString = "Retirar " + valor;
                    sendPacket = new DatagramPacket(eleccionString.getBytes(),
                            eleccionString.getBytes().length, IPAddress, puerto);
                    clientSocket.send(sendPacket);
                    break;

                case 2:
                    System.out.println("\n--> HA INGRESADO: ESTADO DE CUENTA");
                    eleccionString = "Saldo";
                    sendPacket = new DatagramPacket(eleccionString.getBytes(),
                            eleccionString.getBytes().length, IPAddress, puerto);
                    clientSocket.send(sendPacket);
                    break;

                case 3:
                    System.out.println("\n--> HA INGRESADO: SALIR");
                    System.out.println("SALIENDO DEL SISTEMA ESPERE UNOS SEGUNDOS...");
                    Thread.sleep(2000);
                    SystemFlag = false;
                    break;

                default:
                    System.out.println("\n--> HA INGRESADO UNA OPCIÓN INCORRECTA");
                    System.out.println("SALIENDO DEL SISTEMA ESPERE UNOS SEGUNDOS...");
                    Thread.sleep(2000);
                    SystemFlag = false;
                    break;
            }
            if (SystemFlag) {
                System.out.println("Sigue en el sistema");
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                String modifiedSentence = new String(receivePacket.getData());
                System.out.println("Mensaje del Servidor: " + modifiedSentence);

            } else {
                break;
            }
        }
        clientSocket.close();

    }
}