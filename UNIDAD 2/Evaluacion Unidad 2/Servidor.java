import java.net.*;
import java.io.*;

public class Servidor {

    private static DatagramSocket serverSocket;
    private static InetAddress IpAddress;
    private static int puerto;
    private static DatagramPacket sendPacket;
    private static String respuesta;
    private static String aux;

    private static Double saldo = 500.0;
    private static Double auxValor;

    public static void main(String[] args) throws SocketException, IOException {
        // DatagramSocket clientSocket = new DatagramSocket();
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];

        DatagramSocket serverSocket = new DatagramSocket(8000);
        System.out.println("CAJERO AUTOMATICO ACTIVO");
        while (true) {

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData());
            String[] parts = null;
            parts = sentence.split(" ");

            IpAddress = receivePacket.getAddress();
            puerto = receivePacket.getPort();

            switch (parts[0]) {
                case "Depositar":
                    System.out.println("Petición del Cliente: Depositar $" + parts[1]);
                    auxValor = Double.parseDouble(parts[1]);
                    saldo = saldo + auxValor;
                    System.out.println("Se ha realizado un deposito de $" + auxValor + "| Saldo Total: " + saldo);

                    respuesta = "Deposito de $" + auxValor + " realizado con exito";
                    sendData = respuesta.getBytes();
                    sendPacket = new DatagramPacket(sendData, sendData.length, IpAddress, puerto);
                    serverSocket.send(sendPacket);

                    break;

                case "Retirar":
                    System.out.println("Petición del Cliente: Retirar $" + parts[1]);
                    auxValor = Double.parseDouble(parts[1]);
                    if (auxValor > saldo) {
                        respuesta = "Fallo al retirar dinero - No posee Saldo sufiente para esta transacción, ingrese otro valor o consulte estado de cuenta";
                        sendData = respuesta.getBytes();
                        sendPacket = new DatagramPacket(sendData, sendData.length, IpAddress, puerto);
                        serverSocket.send(sendPacket);
                    } else {
                        saldo = saldo - auxValor;
                        System.out.println("Se ha realizado un deposito de $" + auxValor + "| Saldo Total: " + saldo);

                        respuesta = "Retiro de $" + auxValor + " realizado con exito";
                        sendData = respuesta.getBytes();
                        sendPacket = new DatagramPacket(sendData, sendData.length, IpAddress, puerto);
                        serverSocket.send(sendPacket);

                        break;
                    }

                case "Saldo":
                    System.out.println("Petición del Cliente: Estado de Cuenta");
                    String respuesta = "Estado de Cuenta: $"+saldo;
                    sendData = respuesta.getBytes();
                    sendPacket = new DatagramPacket(sendData, sendData.length, IpAddress, puerto);
                    serverSocket.send(sendPacket);

                    break;

                default:
                    break;
            }

        }
    }
}
