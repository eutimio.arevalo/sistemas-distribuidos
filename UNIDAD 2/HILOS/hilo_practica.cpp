#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 100

void *inc_x(void *x_void_ptr);

int main(){
	int x = 0;
	int y = 0;
    clock_t t1, t2, t3, t4 ;
	printf("x: %d, y: %d\n", x, y); // imprimir los valores de x y y
    //HILO
    pthread_t inc_x_thread;
    t1 = clock();
	pthread_create(&inc_x_thread, NULL,&inc_x, &x);
	//JOIN
    pthread_join(inc_x_thread, NULL);
	t2 = clock();
    
    t3 = clock();
    while (++y <MAX);
    t4 = clock();
    printf("y terminó el incremento\t");
	printf("y: %d \n\n", y);
	printf("RESULTADOS:\tX: %d, Y: %d\n", x, y);
    printf("Tiempo total usando hilos fue: %f\n",(((float)t2 - (float)t1) /1000000.0F) * 1000);
    printf("Tiempo total sin hilos fue: %f\n",(((float)t4 - (float)t3) /1000000.0F) * 1000);
	return 0;
}

void *inc_x(void *x_void_ptr){
	int *x_ptr = (int *)x_void_ptr;
	printf("Estoy en el método implementado\n");
	while (++(*x_ptr) <MAX);
	printf("x terminó el incremento\t");
	printf("x: %d \n", *x_ptr);
    return NULL;
}