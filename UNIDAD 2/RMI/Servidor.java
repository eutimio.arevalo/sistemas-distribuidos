import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;

public class Servidor{
    public Servidor(){
        try {
            InterfazRmi objetoRmi = new ImplementacionRmi();
            Naming.rebind("rmi://localhost/oyente", (Remote) objetoRmi);
            System.out.println();
        } catch (Exception ex) {
            System.out.println(""+ex);
        }
    }
    
    public static void main(String[] args) throws RemoteException{
        new Servidor();
    }
}
