
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ClassNotFoundException;
import java.net.ServerSocket;
import java.net.Socket;

public class Cliente {

    private static String host = "127.0.0.1";
    private static int port = 5000;

    public static void main(String args[]) throws IOException, ClassNotFoundException {

        for (int i = 0; i < 10; i++) {
            System.out.println("Iniciando el socket para comunicarnos");
            Socket socket = new Socket(host, port);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("Mi primera vez");
            socket.close();
        }

        //ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        //String message = (String) ois.readObject();
        //ois.close();
    }
}
