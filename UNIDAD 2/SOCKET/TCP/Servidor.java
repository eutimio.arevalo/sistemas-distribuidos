
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ClassNotFoundException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

    private static ServerSocket server;
    private static int port = 5000;

    public static void main(String args[]) throws IOException, ClassNotFoundException {

        server = new ServerSocket(port);
        while (true) {
            System.out.println("Esperando petición del cliente");
            Socket socket = server.accept();
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            String message = (String) ois.readObject();
            ois.close();
            socket.close();
            System.out.println("Finalizando Petición");

        }
        //server.close();

    }
}
