
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class udp_server {
    public static void main(String[] args) throws SocketException, IOException {
        //DatagramSocket clientSocket = new DatagramSocket();
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];
        DatagramSocket serverSocket = new DatagramSocket(9876);
        
        
        
        while (true) {
            System.out.println("Servidor Activo");
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData());
            
            /*ENVIO DE MENSAJE*/
            InetAddress IpAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            
            String capitalizedSentece = sentence.toUpperCase();
            sendData = capitalizedSentece.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IpAddress, port);
            serverSocket.send(sendPacket);
         
            
            System.out.println("Mensaje del Cliente: " + sentence);
            System.out.println("Servidor Deshabilitado");
        }
    }
}
