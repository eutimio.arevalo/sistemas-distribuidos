#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

pthread_mutex_t mutex;

void *factorial(void *valor_x);
int total = 0;


int main(){
	int x = 0;
    int valor = 0;
    clock_t t1, t2;

    printf("Ingresa el máximo de factoriales a calcular: \n"); //Ingreso del valor máximo de factorial
    scanf("%d", &x);
    printf("Nro. total de factoriales: %d\n\n", x);
    //HILOS
    pthread_t hilos[x];
    
    for (int i = 1; i <= x; i++)
    {
        printf("Hilo %d\n", i);
        t1 = clock();
        pthread_create(&hilos[i-1], NULL, &factorial, &i);
        pthread_join(hilos[i-1], NULL);
        t2 = clock();
        //JOIN
        
        printf("Tiempo total usando en el Hilo %d fue: %f\n", i ,(((float)t2 - (float)t1) /1000000.0F) * 1000);
    }
    printf("\n\nTOTAL: %d\n\n", total);
	return 0;
}

void *factorial(void *valor_x){
    int *x_ptr = (int *)valor_x;
    int res = 1;
    for (int i = 1; i <= (*x_ptr); i++)
    {
        res = res * i;
    }
    printf("El factorial de %d es %d\n", *x_ptr, res);

    total = total + res;

    return NULL;
}
