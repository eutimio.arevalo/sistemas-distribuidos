import java.io.*;
import java.net.*;

public class Cliente {

    private static int puerto = 8000;
    private static String host = "127.0.0.7";
    private static Socket socket_cliente;
    private static String ipFalsa = ipAleatoria();
    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
        System.out.println(" ========== C L I E N T E --> "+ ipFalsa +" ===========");
        socket_cliente = new Socket(host, puerto);
        try (PrintWriter salida = new PrintWriter(socket_cliente.getOutputStream(), true);){
            int cont = 1;
            while(true){
                salida.println("Cliente ["+ipFalsa+"] - Mensaje "+cont);
                System.out.println("Mensaje enviado al servidor: "+cont);
                Thread.sleep(500);
                cont++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int numeroAleatorio() {
        // Método que devuelve un número aleatorio entre 0 y 255
        return (int) (Math.random()*256);
      }
    
    public static String ipAleatoria() {
        StringBuilder sbIP = new StringBuilder();
        for (int x=0; x<3; x++) {
            sbIP.append(String.valueOf(numeroAleatorio())).append(".");
        }
        sbIP.append(String.valueOf(numeroAleatorio()));
        return sbIP.toString();
    }

}

