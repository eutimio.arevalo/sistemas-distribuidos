import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ServidorMulticast {

    private static ServerSocket serverSocket;
    private static Socket clientes;

    public static void main(String[] args) throws SocketException, IOException {
        System.out.println(" ========== S E R V I D O R ===========");
        try {
            serverSocket = new ServerSocket(8000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        while (true) {
            clientes = serverSocket.accept();
            Thread hilo = new Thread() {
                public void run() {
                    try (Scanner scanner = new Scanner(clientes.getInputStream());) {
                        while (scanner.hasNextLine()) {
                            String mensaje_cliente = scanner.nextLine();
                            System.out.println(" -> Se ha recibido un mensaje: "+mensaje_cliente);
                         }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            hilo.start();
        }
    }
}
