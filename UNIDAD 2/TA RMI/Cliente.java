import java.rmi.Naming;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        try {
            InterfazRmi interfaz = (InterfazRmi)Naming.lookup("rmi://localhost/banco");
            System.out.println("====== BIENVENIDO AL BANDO AREVALO =======");
            while(true){
                System.out.println("SELECCIONE UNA DE LAS SIGUIENTES OPCIONES:");
                System.out.println("0 => Depositar || 1 => Retirar || 2 => ESTADO DE CUENTA");
                Scanner scan = new Scanner(new InputStreamReader(System.in));
                int eleccion = -1;
                try {
                    eleccion = scan.nextInt();
                } catch (Exception e) {
                    System.out.println("Error al seleccionar, saliendo del sistema...");
                    break;
                }

                float valor = 0;

                if(eleccion == 0){
                    System.out.println("INGRESE VALOR A DEPOSITAR: ");
                    scan = new Scanner(new InputStreamReader(System.in));
                    valor = scan.nextFloat();
                    System.out.println(interfaz.depositar(valor));
                }else if(eleccion == 1){
                    System.out.println("INGRESE VALOR A RETIRAR");
                    scan = new Scanner(new InputStreamReader(System.in));
                    valor = scan.nextFloat();
                    System.out.println(interfaz.retirar(valor));
                }else if(eleccion == 2){
                    System.out.println(interfaz.mirarSaldo());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
