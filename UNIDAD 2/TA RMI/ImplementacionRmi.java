import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class ImplementacionRmi extends UnicastRemoteObject implements InterfazRmi{

    private Float saldo = 500.00f;

    public ImplementacionRmi() throws RemoteException{
        super();
    }
    
    public String depositar(Float cantidad) throws RemoteException{
        saldo = saldo + cantidad;
        return "Se depositó $"+cantidad;
    }

    public String retirar(Float cantidad) throws RemoteException{
        if(cantidad > saldo){
            return "No puede retirar";
        }else{
            saldo = saldo - cantidad;
            return "Se retiró $"+cantidad;
        } 
        
    }

    public String mirarSaldo() throws RemoteException{
        return "Su estado de cuenta es de: "+saldo;
    }
}