import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfazRmi extends Remote{
    public String depositar(Float cantidad) throws RemoteException;
    public String retirar(Float cantidad) throws RemoteException;
    public String mirarSaldo() throws RemoteException;
}
