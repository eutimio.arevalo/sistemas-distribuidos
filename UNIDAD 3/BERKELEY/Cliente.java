import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cliente {

    private static int puerto = 3000;
    private static String host = "localhost";
    private static Socket socket_cliente;

    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
        System.out.println(" ========== C L I E N T E ===========");
        DateFormat formato = new SimpleDateFormat("HH:mm:ss");
        socket_cliente = new Socket(host, puerto);
        try (PrintWriter salida = new PrintWriter(socket_cliente.getOutputStream(), true);) {
            long tiempo = System.currentTimeMillis() + (int) (Math.random() * 30000 + 10000);
            salida.println(String.valueOf(tiempo));
            System.out.println("Mensaje enviado al servidor: " + formato.format(new Date(tiempo)));
            // while (true) {
                
            //     // Thread.sleep(500);
            //     // cont++;
            // }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
