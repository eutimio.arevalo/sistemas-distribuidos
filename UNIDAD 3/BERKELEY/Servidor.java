import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Servidor {

    private static ServerSocket serverSocket;
    private static Socket clientes;
    private static long tiempo_servidor_0;
    private static long tiempo_servidor_1;
    private static long tiempo_cliente;
    private static long tiempo_d;
    private static long tiempo_d_prima;
    private static long tiempo_final_d;
    private static long tiempo_a;
    private static int cont;
    private static long suma = 0;

    public static void main(String[] args) throws SocketException, IOException {
        System.out.println(" ========== S E R V I D O R ===========");
        try {
            serverSocket = new ServerSocket(3000);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        DateFormat formato = new SimpleDateFormat("ss");
        tiempo_servidor_0 = System.currentTimeMillis();
        
        System.out.println("Tiempo Servidor: " + formato.format(new Date(tiempo_servidor_0))); 
        while (true) {
            clientes = serverSocket.accept();
            cont += 1;
            System.out.println("\nContador"+cont+"\n");
            Thread hilo = new Thread() {
                public void run() {
                    try (Scanner scanner = new Scanner(clientes.getInputStream());) {
                        // String mensaje_cliente = scanner.nextLine();  
                        while (scanner.hasNextLine()) {
                                tiempo_cliente = Long.parseLong(scanner.nextLine());
                                System.out.println(" -> Tiempo Cliente: "+formato.format(new Date(tiempo_cliente)));
                                tiempo_d = tiempo_cliente-tiempo_servidor_0;
                                System.out.println("D: "+formato.format(new Date(tiempo_d)));
                                tiempo_servidor_1 = tiempo_servidor_0+10000;
                                System.out.println("Tiempo Servidor Actualizado: "+formato.format(new Date(tiempo_servidor_1)));
                                tiempo_d_prima = tiempo_d - ((tiempo_servidor_1-tiempo_servidor_0)/2);
                                System.out.println("D': "+formato.format(new Date(tiempo_d_prima)));
                                suma = suma + tiempo_d;
                                if(cont==3){
                                    tiempo_final_d = (suma)/4;
                                    System.out.println("\nD_final: "+formato.format(new Date(tiempo_final_d)));
                                    tiempo_a = tiempo_a-tiempo_d_prima;
                                    System.out.println("A: "+formato.format(new Date(tiempo_a)));
                                }
                                
                                // System.out.println(suma+"\n");
                                
                        }
                        
                        // System.out.println("Tiempo Servidor: " + formato.format(new Date(tiempo_servidor_1))); 
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            
            hilo.start();
        }
        
    }
}