import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

public class Cliente {
    
    public static String host = "127.0.0.1";
    public static int puerto = 8000;
    

    public static void main(String[] args) {
        try (
            Socket socket = new Socket(host,puerto);
            PrintWriter print_writer = new PrintWriter(socket.getOutputStream(),true);
            BufferedReader buffered_reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ){
            System.out.println("---- CLIENTE INICIADO CORRECTAMENTE ----");
            long tiempo_cero;
            long tiempo_servidor;
            long tiempo_uno;
            long tiempo_final;

            print_writer.println(tiempo_cero=System.currentTimeMillis()+10000);
            tiempo_servidor = Long.parseLong(buffered_reader.readLine());
            tiempo_uno = System.currentTimeMillis()+30000;
            tiempo_final = tiempo_servidor + ((tiempo_uno-tiempo_cero)/2);
            DateFormat formato = new SimpleDateFormat("HH:mm:ss");

            System.out.println("TIEMPO CLIENTE => "+formato.format(new Date(tiempo_cero)));
            System.out.println("TIEMPO SERVIDOR => "+formato.format(new Date(tiempo_servidor-10000))+"\n");

            System.out.println("\n+++++++++++++++++++++++++++++++\n");
            System.out.println("Tiempo Final = "+formato.format(new Date(tiempo_servidor))+" + ("+formato.format(new Date(tiempo_uno))+" - "+formato.format(new Date(tiempo_cero))+")/2");
            System.out.println("Tiempo Final = "+formato.format(new Date(tiempo_final)));
            System.out.println("\n+++++++++++++++++++++++++++++++\n");

            System.out.println("TIEMPO CLIENTE LUEGO DE APLICAR CRISTIAN => "+formato.format(new Date(tiempo_final))+"\n");
            print_writer.println("Finalización del algoritmo...");

        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Ha ocurrido un error\n"+e.getMessage());
        }
    }
}