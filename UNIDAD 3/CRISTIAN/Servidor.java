import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

public class Servidor {
    public static String host = "127.0.0.1";
    public static int puerto = 8000;

    public static void main(String[] args) {
        try (
            ServerSocket serverSocket = new ServerSocket(puerto);
            Socket clientSocket = serverSocket.accept();
            PrintWriter print_writer = new PrintWriter(clientSocket.getOutputStream(),true);
            BufferedReader buffered_reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); 
              
        ){
            System.out.println("---- SERVIDOR INICIADO CORRECTAMENTE ----");
            DateFormat formato = new SimpleDateFormat("HH:mm:ss"); 
            System.out.println("Tiempo servidor:"+formato.format(new Date(System.currentTimeMillis())));
            while (true) {
               print_writer.println(System.currentTimeMillis()+10000); 
            }
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Ha ocurrido un error:\n"+e.getMessage());
        }
    }
}
