numero_procesos = int(input(('Ingresar número de procesos: ')))
numero_tiempos = int(input(('Ingresar número de tiempos: ')))

# Matriz Dinâmica
procesos = []

for i in range(numero_procesos):
    procesos.append([])
    for j in range(numero_tiempos):
        procesos[i].append(j)

index = 0

#Crear primer proceso

for i in range(1):
    for j in range(numero_tiempos):
        if i == 0 and j == 0:
            procesos[0][0] = 0
        if i != 0 or j != 0:
            procesos[i][j] = index
        index += 5

# Crear procesos según el ingreso de información

diferencia = 0
diferencia_estados = 0

for i in range(numero_procesos):
    if (i == 0):
        continue
    diferencia += 3
    diferencia_estados = 0
    for j in range(numero_tiempos):
        if j == 0:
            procesos[i][j] = 0
        elif j == 1:
            procesos[i][j] = procesos[i-1][j] + 3
            diferencia_estados = procesos[i][j]
        else:
            procesos[i][j] = procesos[i][j-1] + diferencia_estados

#Mostrar procesos creados

for i in range(numero_procesos):
    print('')
    print('P:',i)
    for j in range(numero_tiempos):
        print('[', procesos[i][j], ']', end="")
print('')
print('------------------------------------------------------------')

#Recibir indice del proceso y tiempo a utilizar

proceso_origen = int(input(('Ingresar proceso de origen: ')))
tiempo_origen = int(input(('Ingresar tiempo de origen: ')))
proceso_destino = int(input(('Ingresar proceso de destino: ')))
tiempo_destino = int(input(('Ingresar tiempo de destino: ')))

diferencia = procesos[proceso_destino][1]

#Validar si es necsaria la sincronización

if (procesos[proceso_origen][tiempo_origen] > procesos[proceso_destino][tiempo_destino]):
    procesos[proceso_destino][tiempo_destino] = procesos[proceso_origen][tiempo_origen] + 1
    for i in range(tiempo_destino + 1, numero_tiempos):
        procesos[proceso_destino][i] = procesos[proceso_destino][i-1] + diferencia
    print('Sincronización realizada: ', procesos[proceso_origen][tiempo_origen], '->', procesos[proceso_destino][tiempo_destino])
else:
    print('No es necesario sincornización: ', procesos[proceso_origen][tiempo_origen], '->', procesos[proceso_destino][tiempo_destino])

#Mostrar Proceso sincronizado

print('------------------------------------------------------------')
for i in range(numero_procesos):
    print('')
    print('P:',i)
    for j in range(numero_tiempos):
        print('[', procesos[i][j], ']', end="")
print('')
print('------------------------------------------------------------')






