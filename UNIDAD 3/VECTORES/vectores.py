def comparar_vectores(vector1,vector2):
    vector = [max(value) for value in zip(vector1,vector2)]
    return vector
    
P = {1:{}, 2:{}, 3:{}} # Inicializar diccionario

cont = 0

puntos_p1 = int(input("INGRESAR PUNTOS DEL PROCESO 1: "))
e1 = [i for i in range(1, puntos_p1 + 1)]
P[1] = {key: [cont + key, 0, 0] for key in e1}

puntos_p2 = int(input("INGRESAR PUNTOS DEL PROCESO 2: "))
e2 = [i for i in range(1, puntos_p2 + 1)]
P[2] = {key: [0, cont + key, 0] for key in e2}

puntos_p3 = int(input("INGRESAR PUNTOS DEL PROCESO 3: "))
e3 = [i for i in range(1, puntos_p3 + 1)]
P[3] = {key: [0, 0, cont + key] for key in e3}

print("\n")
print("P1:\n",P[1])
print("P2:\n",P[2])
print("P3:\n",P[3])
print("\n")

cantidad_lineas = int(input("Ingresar cantidad de lineas dentro del algoritmo: "))
print("\n")

while cont < cantidad_lineas:
    print(" --- LINEA ",cont+1," --- \n")

    proceso_origen = int(input("Ingresar proceso de origen: "))
    proceso_destino = int(input("Ingresar proceso de destino: "))
    punto_origen = int(input("Ingresar index del punto de envio: "))
    punto_destino = int(input("Ingresar index del punto de destino: "))

    if proceso_origen <= 3 and proceso_destino <= 3:

        print ("P",proceso_origen," --> P",proceso_destino)
        new_vector = comparar_vectores(P[proceso_origen][punto_origen],P[proceso_destino][punto_destino])
        P[proceso_destino][punto_destino] = new_vector
        print ("Actualización del vector: Punto",punto_destino,"  en el proceso P",proceso_destino," => ",P[proceso_destino][punto_destino]," \n")

        # Actualización de vectores
        if (punto_destino + 1) in P[proceso_destino]:

            for i in range(punto_destino + 1, len(P[proceso_destino]) + 1):
                P[proceso_destino][i] = comparar_vectores(P[proceso_destino][i-1],P[proceso_destino][i])

    else:
        print ("Ingrese datos correctamente")


    cont += 1
    

print("RESULTADO:\n")
print("P1:\n",P[1])
print("P2:\n",P[2])
print("P3:\n",P[3])
print("\n")

